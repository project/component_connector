## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Recommended Modules
- Installation
- Configuration

## INTRODUCTION

The "Component Connector" module is a Drupal module that facilitates the integration of external components and layouts into the current theme.
It allows developers to define and register component definitions using YAML files, which can include custom elements, libraries and variables.

The module provides the following key features:

Dynamic Component Integration: Developers can define and manage component definitions in separate YAML files.
These definitions can include various properties like hook theme, attached libraries, custom fields and/or settings to be passed into render.

Library Management: The module handles the inclusion of CSS and JS libraries for each component, ensuring proper asset loading and dependency management.

- For a full description of the module visit:
  <https://www.drupal.org/project/component_connector/README.md>

- To submit bug reports and feature suggestions, or to track changes
  visit: <https://www.drupal.org/project/issues/component_connector>

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## RECOMMENDED MODULES

We recommend to use [UI Patterns](https://www.drupal.org/project/ui_patterns) module

## INSTALLATION

- Install the Component Connector module as you would normally install a
  contributed Drupal module. Visit
  <https://www.drupal.org/docs/extending-drupal/installing-modules> for
  further information.

## CONFIGURATION

Once you installed module, please define theme machine name in `component_connector.settings.yml`.
You may do it on settings form `/admin/config/system/component_connector_settings`

First, create "components" including custom css/js in your theme folder, and organize them in your preffered way, eg:
```
my_theme/
|-- templates/
|-- |-- components/
|   |   |-- my-component/
|   |   |   |-- my-component.html.twig
|   |   |   |-- my-component.css
|   |   |   |-- my-component.js
|   |   |
|   |   |-- another-component/
|   |   |   |-- another-component.html.twig
|   |   |   |-- another-component.css
|   |   |   |-- another-component.js
```

Next step is to integrate your components into Drupal.
Components can be integrated into drupal using yml file `my-component.theme.yml` or `my-component.suggestion.yml`:

To define component properties using YAML files for the Component Connector module, the following structure requirements should be followed:

1. Component Definition YAML Structure:

```yaml
component_name: # Unique identifier for the component
  label: 'Component Label' # A user-friendly label for the component
  hook theme: 'hook_theme_name' # Drupal hook theme name for rendering the component
  base hook: 'base_hook_name' # Base hook theme name for inheritance (optional)
  fields: # Additional variables or render elements for the component (optional)
    field_name:
      label: 'Field Label'
      # Other field properties as needed
  settings: # Additional settings for the component (optional)
    setting_name: 'Setting Value'
  libraries: # CSS and JS libraries required for the component (optional)
    - library-name: # Unique identifier for the library, use "component_name"
        css: # List of CSS files for the library (optional)
          - 'file.css'
        js: # List of JS files for the library (optional)
          - 'file.js'
```

2. Suggestion Definition YAML Structure:
```yaml
suggestion_name: # Unique identifier for the suggestion
  base hook: 'base_hook_name' # Base hook theme name for inheritance
  fields: # Additional variables or render elements for the suggestion (optional)
    field_name:
      label: 'Field Label'
      # Other field properties as needed
  settings: # Additional settings for the suggestion (optional)
    setting_name: 'Setting Value'
```

Note:

- The `component_name` and `suggestion_name` should be unique identifiers for each component and suggestion, respectively.
- The `base hook` property in the component definition indicates inheritance from an existing hook theme.
- The `hook theme` in the component definition specifies the Drupal hook theme responsible for rendering the component.
- The `libraries` section in the component definition is optional and is used to include CSS and JS libraries for the component.
- The `fields` and `settings` sections in both component and suggestion definitions are optional and can be used to define additional variables or settings for the component or suggestion.

### Types of integrations

- [Custom theme hook](#custom-theme-hook)
- [Theme hook "candidate"](#theme-hook-candidate)
- [Replace existing theme hook](#replace-existing-theme-hook)
- [Theme hook suggestion](#theme-hook-suggestion)

#### Custom theme hook

This type of integration means you are creating custom `hook_theme` for your component to be used in your code.

Only one line required.
```yml
  hook theme: 'my_progress_percentage'
```

Example of component which should show some "progress" with percentage:

```
my_theme/
|-- templates/
|-- |-- components/
|   |   |-- my-progress-percentage/
|   |   |   |-- my-progress-percentage.html.twig
|   |   |   |-- my-progress-percentage.css
|   |   |   |-- my-progress-percentage.js
|   |   |   |-- my-progress-percentage.theme.yml
```

1. **my-progress-percentage.theme.yml**

```yml
my_progress_percentage:
  label: My progress percentage
  hook theme: 'my_progress_percentage'
  fields:
    value:
      label: Value
  libraries:
    - my-progress-percentage:
        css:
          component:
            my-progress-percentage.css: {}
```

2. **my-progress-percentage.html.twig**

```html
<div class="my-progress-percentage">
  <div class="my-progress-percentage__active" style="width: {{ value }}%;"></div>
</div>
```

3. **my-progress-percentage.css**

```css
.my-progress-percentage {
  background-color: #f0f0f0;
}
```
4. **my-progress-percentage.js**

```js
(function ($) {
  Drupal.behaviors.myProgressPercentage = {
    attach: function (context, settings) {
      // Your component-specific JavaScript logic here
    }
  };
})(jQuery);
```

And then in your module, eg in field formatter for example you may use new hook theme.

File `my_module/src/Plugin/Field/FieldFormatter/ProgressFormatter.php`
```php
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    foreach ($items as $delta => $item) {
      $definition = $item->getDataDefinition();
      $element[$delta] = [
        '#theme' => 'my_progress_percentage',
        '#value' => $item->value,
      ];
    }
    return $element;
  }
```

#### Theme hook "candidate"

This type of integration means you are creating custom `hook_theme` for your component based on some existing hook_theme from core or contributed module.

Two lines required.
```yml
  hook theme: 'input__checkbox'
  base hook: 'input'
```

Drupal core provides mechanism to extend existing hook_theme.

Core implementation see https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/lib/Drupal/Core/Theme/ThemeManager.php#L163

So in some cases core/contrib code allow you to use "candidates", examples:
* Password element https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/lib/Drupal/Core/Render/Element/Password.php#L48
* Email element https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/lib/Drupal/Core/Render/Element/Email.php#L64
* Menu builder https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/lib/Drupal/Core/Menu/MenuLinkTree.php#L183
* And others.

Example: We are creating component for input checkbox.

```
my_theme/
|-- templates/
|-- |-- components/
|   |   |-- my-checkbox/
|   |   |   |-- my-checkbox.html.twig
|   |   |   |-- my-checkbox.css
|   |   |   |-- my-checkbox.js
|   |   |   |-- my-checkbox.theme.yml
```


1. **my-checkbox.theme.yml**

```yml
my_checkbox:
  label: My checkbox
  hook theme: 'input__checkbox'
  base hook: 'input'
  libraries:
    - my-checkbox:
        css:
          component:
            my-checkbox.css: {}
        js:
          my-checkbox.js: {}
```

Provide `my-checkbox.html.twig` and add styles/js if needed:

* **my-checkbox.css**
* **my-checkbox.js**

#### Replace existing theme hook

This type of integration replace existing core/contrib hook with your own implementation.

Only one line required.
```yml
  hook theme: 'form_element_label'
```

Example: We are replacing component for "form element label".

```
my_theme/
|-- templates/
|-- |-- components/
|   |   |-- my-form-element-label/
|   |   |   |-- my-form-element-label.html.twig
|   |   |   |-- my-form-element-label.css
|   |   |   |-- my-form-element-label.js
|   |   |   |-- my-form-element-label.theme.yml
```

1. **my-form-element-label.theme.yml**

```yml
my_form_element_label:
  label: My form element label
  hook theme: 'form_element_label'
  libraries:
    - my-form-element-label:
        css:
          component:
            my-form-element-label.css: {}
        js:
          my-form-element-label.js: {}
```

Provide `my-form-element-label.theme.html.twig` and add styles/js if needed.

**Important notice**

Drupal render mechanism allow us to use render element OR variables, inside hook theme, but not both.
* see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21theme.api.php/function/hook_theme/10
* and core implementation https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/lib/Drupal/Core/Theme/ThemeManager.php#L191

So when you extend/replace any hook theme, please be sure you are not using `fields/settings` properties in your yml.
Also check initial `hook_theme` implementation/twig, to be sure you are using same variables or render element.


#### Theme hook "suggestion"

This type allow you to use your template as **suggestion** and extends render array with additional variables.
Name of yml file in this case must be **my_component.suggestion.yml**.
In this case we replace reference to `html.twig` from core registry and replace it with our `component.html.twig` file.
Also this type of integration give you availability to use additional variables, which may be defined in your `yml`.
All `fields/settings` are passed to hook theme in variables section, so we avoid limitation described in previous section.

Only one line required.
```yml
  hook theme: 'pager'
```

Example: pager element.

```
my_theme/
|-- templates/
|-- |-- components/
|   |   |-- my-pager/
|   |   |   |-- my-pager.html.twig
|   |   |   |-- my-pager.css
|   |   |   |-- my-pager.js
|   |   |   |-- my-pager.suggestion.yml
```

1. **my-pager.suggestion.yml**

```yml
my_pager:
  label: My pager
  base hook: 'pager'
  libraries:
    - my-pager:
        css:
          component:
            my-pager.css: {}
```

So core hook_theme `pager` will use `my-pager.html.twig` file for render, so be sure you are using same structure from initial definition https://git.drupalcode.org/project/drupal/-/blob/10.1.x/core/includes/theme.inc#L2038


#### Theme hook layout

This type of integration is similar to Theme hook "candidate", but “base hook” property should always has value “layout”

```yml
h_container:
  label: Layout / Container
  hook theme: 'h_container'
  base hook: 'layout'
  fields:
    content:
    type: render
    label: Content
```

In this case the new layout “Layout / Container” will be registered in Drupal theme registry and becomes available in the admin UI, where it's allowed, in panels, entity view modes, etc. make sure you define the “fields” section, it will be transformed into “regions” property. 

See details: https://www.drupal.org/docs/drupal-apis/layout-api/how-to-register-layouts 