<?php

namespace Drupal\component_connector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Component Drupal settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ThemeHandlerInterface $theme_handler) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'component_connector_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['component_connector.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $theme = $this->config('component_connector.settings')->get('theme');
    $theme_options = [
      '' => $this->t('- None -'),
    ];
    /** @var \Drupal\Core\Extension\Extension $theme_info */
    foreach ($this->themeHandler->listInfo() as $theme_name => $theme_info) {
      if (!empty($theme_info->status)) {
        $theme_options[$theme_name] = $theme_info->info['name'];
      }
    }
    $form['theme'] = [
      '#type' => 'select',
      '#options' => $theme_options,
      '#title' => $this->t('Theme'),
      '#required' => FALSE,
      '#default_value' => $theme,
      '#description' => $this->t('Select theme to process components integration.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('component_connector.settings')
      ->set('theme', $form_state->getValue('theme'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
