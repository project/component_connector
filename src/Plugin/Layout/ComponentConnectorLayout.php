<?php

namespace Drupal\component_connector\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Class ComponentConnectorLayout.
 *
 * @package Drupal\component_connector\Plugin\Layout
 */
class ComponentConnectorLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $configuration = $this->getConfiguration();
    // Remove default field template if "Only content" option has been selected.
    if ($configuration['field_templates'] == 'only_content') {
      $this->processOnlyContentFields($regions);
    }
    return parent::build($regions);
  }

  /**
   * Remove default field template if "Only content" option has been selected.
   *
   * @param array $regions
   *   Layout regions.
   */
  protected function processOnlyContentFields(array &$regions) {
    foreach ($regions as $region_name => $region) {
      if (is_array($region)) {
        if (isset($regions[$region_name]['#prefix'])) {
          unset($regions[$region_name]['#prefix']);
        }
        if (isset($regions[$region_name]['#suffix'])) {
          unset($regions[$region_name]['#suffix']);
        }
        foreach ($regions[$region_name] as $field_name => $field) {
          if (is_array($field) && isset($field['#theme']) && $field['#theme'] == 'field') {
            $regions[$region_name][$field_name]['#theme'] = NULL;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $default_settings = $this->getPluginDefinition()->get('settings');
    if (!empty($default_settings)) {
      foreach ($default_settings as $key => $default_setting) {
        $configuration[$key] = isset($default_setting['default_value']) ?: NULL;
      }
    }
    $configuration['field_templates'] = 'default';
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();
    $form['field_templates'] = [
      '#type' => 'select',
      '#title' => $this->t('Field templates'),
      '#options' => [
        'default' => $this->t("Default"),
        'only_content' => $this->t("Only content"),
      ],
      '#description' => implode('<br/>', [
        $this->t("<b>Default</b>: use field templates to wrap field content."),
        $this->t("<b>Only content</b>: only print field content, without field wrapping or label."),
      ]),
      '#default_value' => $configuration['field_templates'],
    ];
    $this->processSettingsForm($configuration, $form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration = $form_state->getValues();
  }

  /**
   * Process settings from yml.
   *
   * @param array $configuration
   *   Configuration.
   * @param array $form
   *   Form.
   */
  private function processSettingsForm(array $configuration, array &$form) {
    $settings_definitions = $this->getPluginDefinition()->get('settings');
    foreach ($configuration as $key => $config_item) {
      if (isset($settings_definitions[$key])) {
        $form[$key] = [
          '#title' => $settings_definitions[$key]['label'],
          '#default_value' => $config_item,
        ];
        switch ($settings_definitions[$key]['type']) {
          case 'textfield':
            $form[$key]['#type'] = 'textfield';
            break;

          case 'boolean':
            $form[$key]['#type'] = 'checkbox';
            break;

          case 'select':
            $form[$key]['#type'] = 'select';
            $form[$key]['#options'] = $settings_definitions[$key]['options'];
            break;
        }
      }
    }
  }

}
