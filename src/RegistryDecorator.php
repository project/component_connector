<?php

namespace Drupal\component_connector;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Theme\Registry;

/**
 * RegistryDecorator service.
 *
 * @phpstan-ignore-next-line
 */
class RegistryDecorator extends Registry {

  /**
   * The search manager.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The name of the theme.
   *
   * @var string|null
   */
  protected $componentsThemeName;

  /**
   * Set config manager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function setConfigManager(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->componentsThemeName = $this->configFactory->get('component_connector.settings')
      ->get('theme');
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    // Initialize default theme first.
    $this->initDefault();
    return parent::get();
  }

  /**
   * Initialize theme manually.
   */
  public function initDefault() {
    if (!$this->themeHandler->themeExists($this->componentsThemeName)) {
      return;
    }
    $active_theme = $this->themeManager->getActiveTheme();
    if ($this->componentsThemeName && $this->componentsThemeName != $active_theme->getName()
      && !isset($this->registry[$this->componentsThemeName])
      && !$this->cache->get('theme_registry:' . $this->componentsThemeName)) {
      $this->themeManager->setActiveTheme($this->themeInitialization->getActiveThemeByName($this->componentsThemeName));
      $this->init();
      $this->build();
      // Only persist it if all modules are loaded to ensure it is complete.
      if ($this->moduleHandler->isLoaded()) {
        $this->setCache();
      }
      $this->themeManager->setActiveTheme($active_theme);
    }
  }

}
